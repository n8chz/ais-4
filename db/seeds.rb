# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

[
  "There is an upper limit (such as Dunbar's number) on the size of groups that can self-govern on a consensus basis.",
  "All economic actors (including the government) are price takers—there is no such thing as a price maker",
  "All private entities are price takers.",
  "At least in the eyes of the larger society, the transition from childhood to adulthood should be instantaneous.",
  "Constituted authority is the ultimately the last line of defense against de-facto power.",
  "Economics is a human invention.",
  "Human social groups are inherently hierarchical.",
  "It takes a village to raise a child.",
  "Left to their own devices, human beings will slaughter each other.",
  "Legitimate authority is a matter of expertise—the bootmaker is an authority concerning boots.",
  "People from underprivileged minorities need to be offered advantages to compensate for their underprivileged status.",
  "Self interest could be realized against others' freedom.",
  "Self interest is an inherent feature of human nature.",
  "Some childhood imprints are so deep as to be immutable.",
  "Some parts of human experience are outside economics.",
  "Territorialism is a component of human nature.",
  "That which is not 100% coerced is 100% voluntary, and vice versa, i.e. there is no in-between—every action is one or the other.",
  "The Law of Fives is correct.",
  "There is no possibility of a method for allocating economic resources that can be more efficient than the market mechanism.",
  "The phenomenon called biological sex imposes hard constraints on knowledge and experience.",
  "The potential wants of individuals for economic goods are limitless.",
  "There are aspects of reality that are truly transcendent.",
  "There are no inherent features of human nature, i.e. there are as many types of human nature as there are humans.",
  "There are some contexts in which fair competition between humans of differing sexes is impossible.",
  "There is more power in government than there is in business.",
  "There is no such thing as legitimate authority.",
  "Without widespread belief in the truth of some religion, life would be very unsafe."
].each do |question_text|
  Question.create [question: question_text]
end
