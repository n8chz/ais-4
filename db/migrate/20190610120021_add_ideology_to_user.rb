class AddIdeologyToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :ideology, :string
  end
end
