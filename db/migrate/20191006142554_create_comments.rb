class CreateComments < ActiveRecord::Migration[6.0]
  def change
    create_table :comments do |t|
      t.references :question, null: false, foreign_key: true
      t.references :profile, null: false, foreign_key: true
      t.text :comment

      t.timestamps
    end
  end
end
