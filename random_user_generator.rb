def random_alpha_string(length_range=2..10)
  Array.new(rand(length_range)) {("a".."z").to_a.sample}.join("")
end

def random_email_address
  random_alpha_string << "@" << random_alpha_string << "." << random_alpha_string(2..3)
end

def users_without_profiles
  User.all.select {|u| Profile.find_by(user_id: u.id).nil?}
end

def create_profile_data
  nq = Question.count
  users_without_profiles.each do |u|
    Profile.new(user_id: u.id, ideology: random_alpha_string).save
    prof_id = Profile.find_by(user_id: u.id).id
    Question.all.sample(rand(1..nq)).each do |q|
      Answer.new(profile_id: prof_id, question_id: q.id, score: rand(-2..2)).save
    end
  end
end
