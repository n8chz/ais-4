json.extract! answer, :id, :profile_id, :question_id, :score, :created_at, :updated_at
json.url answer_url(answer, format: :json)
