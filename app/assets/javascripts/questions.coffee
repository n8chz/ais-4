# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

console.log "questions.coffee checking in"

$ ->
  button = $ ".flag"
  # auth = encodeURIComponent button.attr("data-authenticity_token")
  button.each ->
    # console.log "#{$(this).get(0).outerHTML}"
    this_button = $(this)
    question = this_button.attr("data-question-id")
    user = this_button.attr("data-user-id")
    # console.log "question=#{question}, user=#{user}"
    $(this).click ->
      # console.log "in click handler"
      text = $(this).text()
      Rails.ajax
        # h/t Muhammad Adeel Zahid https://stackoverflow.com/a/7569266/948073
        url: "/#{text}/#{question}/#{user}"
        type: "post"
        success: (data) ->
          this_button.text data
