class Question < ApplicationRecord
  def correlation_with(other)
    # TODO: make less ugly if possible
    others_profiles = Answer.where(question: other).map(&:profile)
    profiles = Answer.where(question: self, profile: others_profiles).map(&:profile)
    n, sx, sy, sxy, sx2, sy2 = [0, 0, 0, 0, 0, 0]
    profiles.each do |p|
      x = Answer.where(profile: p.id, question: id)[0].score
      y = Answer.where(profile: p.id, question: other)[0].score
      n += 1
      sx += x
      sy += y
      sxy += x*y
      sx2 += x*x
      sy2 += y*y
    end
    (n*sxy-sx*sy)/Math.sqrt((n*sx2-sx*sx)*(n*sy2-sy*sy))
  end

  def correlations_to_others
    Question.where.not(id: self.id).map do |q|
      # next if q.id == id
      c = correlation_with(q)
      next if c.nan?
      {id: q.id, question: q.question, correlation: c}
    end.reject(&:nil?).group_by do |props|
      props[:correlation].positive? ? :positive : :negative
    end
  end

  def self.correlations
    Question.all.to_a.combination(2).map do |q1, q2|
      [q1.id, q2.id, q1.correlation_with(q2)]
    end.reject {|_, _, c| c.nan?}
  end
end
