class Profile < ApplicationRecord
  belongs_to :user

  def compatibility(other)
    self_answers = Answer.where(profile_id: id).to_a
    other_answers = Answer.where(profile_id: other.id).to_a
    self_answers.product(other_answers).map do |x, y|
      x.score*y.score
    end.sum
  end

  def compatibilities
    Profile.all.map do |p|
      {
        id: p.id,
        ideology: p.ideology,
        compatibility: self.compatibility(p)
      }
    end.sort_by {|p| p[:compatibility]}
  end
end
