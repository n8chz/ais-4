class QuestionsController < ApplicationController
  before_action :set_question, only: [:show, :edit, :update, :destroy]

  def index
    if user_signed_in?
      profile = Profile.find_by(user: current_user)
      @questions = Answer.where(profile: profile).map(&:question).sample(30) # TODO: Make 30 something in site settings
      redirect_to "/" if @questions.empty?
    else
      redirect_to "/"
    end
  end

  def new
    if user_signed_in?
      @question = Question.new
    else redirect_to new_user_session_path
    end
  end

  def show
    @question = Question.find(params[:id])
  end

  def create
    @question = Question.new(question_params)
    @question.user_id = current_user.id
    respond_to do |format|
      if @question.save
        format.html { redirect_to @question, notice: "Added response item."}
      else
        format.html { render :new }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question
      @question = Question.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_params
      params.require(:id).permit(:user_id, :id, :question)
    end
end
