class FlagsController < ApplicationController
  def create
    @flag = Flag.new(flag_params)
    respond_to do |format|
      if @flag.save
        format.json { render json: "unflag" }
      else
        format.json { render json: "error" }
      end
    end
  end

  def destroy
    Flag.find_by(flag_params).destroy
    respond_to do |format|
      format.json { render json: "flag" }
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def flag_params
      params.permit(:id, :user_id, :question_id)
    end
end
