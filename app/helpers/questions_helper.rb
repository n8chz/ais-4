module QuestionsHelper
  def questions_map
  end

  def correlation_list_for c
    content_tag :td, class: :top_align do
      if c
        c.sort_by {|q| -q[:correlation].abs}.map do |q|
          content_tag(:span, q[:correlation].to_s+" ", class: :corr) <<
          # link_to(q[:question], "/question/#{q[:id]}") <<
          link_to(q[:question], question_path(q[:id])) <<
          content_tag(:br)
        end.join("").html_safe
      end
    end
  end

  def question_correlation q
    c = q.correlations_to_others
    content_tag(:tr) do
      correlation_list_for(c[:positive])+correlation_list_for(c[:negative])
    end.html_safe
  end

  def flag_button u, q
    puts "user_id: "#{u.id}"
    flag = Flag.find_by(question: q, user: u)
    button_data = {user_id: u.id, question_id: q.id}
    puts "button_data: #{button_data}"
    button_tag flag ? "unflag" : "flag", data: button_data, class: "flag"
  end
end
