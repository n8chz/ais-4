module ProfilesHelper
  # Generate a cell of the compatibility table in views/profiles/show
  def compatibility_ranking_cell p
    content_tag(:td, link_to(p[:ideology], profile_path(p[:id])))
  end

  # Generate a row of the compatibility table in views/profiles/show
  def compatibility_ranking_row allies, rivals, i
    cells = compatibility_ranking_cell(allies[i])
    cells << compatibility_ranking_cell(rivals[i])
    content_tag :tr, cells
  end

  # Generate highest-variance 2D visualization of profiles
  # using PCA
  def profiles_map
    labels = Profile.all.map(&:ideology)
    data = pivot_table Profile, Question, Answer, :score
    visualize labels, data
  end

  # agglomerative clustering of profiles, by compatibility
  def profile_clusters
    profile_ids = Profile.all.map(&:id)
    compatibility_table = Profile.all.to_a.combination(2).map do |p1, p2|
      [[p1.id, p2.id], p1.compatibility(p2)]
    end.to_h
    points = pivot_table Profile, Question, Answer, "score"
    points_table = profile_ids.zip(points).to_h
    clusters = profile_ids.map {|x| [x]}
    # clusters = [[10, 12, 14, 17, 20, 22, 24], [13, 15, 16, 23], [18, 21], [19]]
    (2...clusters.count).map do
      centroids_table = clusters.map do |cluster|
        [cluster, centroid(cluster.map {|p| points_table[p]})]
      end.to_h
      # currently returns hash mapping clusters to their centroids
      clusters_to_merge = clusters.combination(2).min_by do |c1, c2|
        centroids_table[c1].zip(centroids_table[c2]).map do |s1, s2|
          (s1-s2)**2
        end.sum
      end
      new_cluster = clusters_to_merge.flatten
      clusters = clusters-clusters_to_merge+[clusters_to_merge.flatten]
      # recalculate centroids_table
      clusters_to_merge.each {|c| centroids_table.delete c}
      centroids_table[new_cluster] = centroid(new_cluster.map {|p| points_table[p]})
      data = compatibility_table.map do |profile_pair, compatibility|
        comrades = profile_pair.map do |p|
          clusters.find_index{|c| c.include? p}
        end.reduce(:==) ? 1 : 0
        [compatibility, comrades]
      end
      n, sx, sy, sxy, sx2, sy2 = [0, 0, 0, 0, 0, 0]
      data.each do |x, y|
        n += 1
        sx += x
        sy += y
        sxy += x*y
        sx2 += x*x
        sy2 += y*y
      end
      corr = (n*sxy-sx*sy)/Math.sqrt((n*sx2-sx*sx)*(n*sy2-sy*sy))
      [clusters, corr]
    end.max_by {|_, corr| corr}.first
  end

  private
  def centroid(cluster_points)
    cluster_points.transpose.map do |q|
      q.sum.to_f/q.count
    end
  end
end
