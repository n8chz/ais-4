module ApplicationHelper
  # Reduce dimensionality to 2 for visualization
  def visualize labels, data
    pca = PCA.new components: 2 # see https://github.com/gbuesing/pca#example-usage 
    data_2d = pca.fit_transform(data).to_a
    puts "data_2d: #{data_2d}"
    puts "data_2d.transpose: #{data_2d.transpose}"
    x_min, x_max, y_min, y_max = data_2d.transpose.map(&:minmax).flatten
    content_tag(:div, class: :map) do
      labels.zip(data_2d).map(&:flatten).map do |label, x, y|
        xx = 512.0*(x-x_min)/(x_max-x_min)
        yy = 512.0*(y-y_min)/(y_max-y_min)
        content_tag(:span, label, style: "top: #{xx}px; left: #{yy}px;", class: :map_label)
      end.join("").html_safe
    end
  end

  def pivot_table row_model, column_model, pivot_model, pivot_attribute
    row_model.all.map do |r|
      column_model.select(:id).sort.map do |c|
        pivot_model.find_by(
          row_model.to_s.downcase => r,
          column_model.to_s.downcase => c
        )&.public_send(pivot_attribute) || 0
      end
    end
  end
end
