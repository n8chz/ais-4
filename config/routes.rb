Rails.application.routes.draw do
  resources :profiles
  resources :answers
  resources :questions
  # resources :flags
  get 'welcome/index'
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post "/flag/:question_id/:user_id", to: "flags#create"
  post "/unflag/:question_id/:user_id", to: "flags#destroy"
  root to: "welcome#index"
end
