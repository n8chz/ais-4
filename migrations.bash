#!/bin/bash

rails generate scaffold Answer profile:references question:references score:integer
rails generate scaffold Affiliation user:references profile:references score:integer
rails generate scaffold Profile user:references handle:string ideology:string
